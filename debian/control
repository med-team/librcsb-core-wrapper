Source: librcsb-core-wrapper
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Laszlo Kajan <lkajan@debian.org>,
           Andreas Tille <tille@debian.org>
Section: libs
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5),
               debhelper-compat (= 13),
               doxygen,
               graphviz,
               dh-sequence-python3,
               libboost-python-dev,
               libxerces-c-dev,
               python3-dev,
               python3-setuptools,
               libtool-bin,
               bison,
               flex
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/librcsb-core-wrapper
Vcs-Git: https://salsa.debian.org/med-team/librcsb-core-wrapper.git
Homepage: https://sw-tools.rcsb.org/apps/CORE-WRAPPER/index.html
Rules-Requires-Root: no

Package: librcsb-core-wrapper0t64
Provides: ${t64:Provides}
Replaces: librcsb-core-wrapper0
Breaks: librcsb-core-wrapper0 (<< ${source:Version})
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: C++ library providing OO API to information in mmCIF format
 The RCSB Core Wrapper library was developed to provide an object-oriented
 application interface to information in mmCIF format. It includes several
 classes for accessing data dictionaries and mmCIF format data files.
 .
 This package contains the shared library.

Package: librcsb-core-wrapper0-dev
Architecture: any
Section: libdevel
Depends: librcsb-core-wrapper0t64 (= ${binary:Version}),
         ${misc:Depends}
Suggests: librcsb-core-wrapper-doc
Provides: librcsb-core-wrapper-dev
Description: development files for librcsb-core-wrapper0t64
 The RCSB Core Wrapper library was developed to provide an object-oriented
 application interface to information in mmCIF format. It includes several
 classes for accessing data dictionaries and mmCIF format data files.
 .
 This package contains files necessary for developing applications with
 the library.

Package: librcsb-core-wrapper-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: documentation for librcsb-core-wrapper0t64
 The RCSB Core Wrapper library was developed to provide an object-oriented
 application interface to information in mmCIF format. It includes several
 classes for accessing data dictionaries and mmCIF format data files.
 .
 This package contains html documentation.

Package: python3-corepywrap
Architecture: any
Section: python
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
         librcsb-core-wrapper0t64 (= ${binary:Version})
Suggests: librcsb-core-wrapper-doc
Provides: ${python3:Provides}
Description: library that exports C++ mmCIF accessors to Python3
 The RCSB Core Wrapper library was developed to provide an object-oriented
 application interface to information in mmCIF format. It includes several
 classes for accessing data dictionaries and mmCIF format data files.
 .
 This library provides Python3 bindings for librcsb-core-wrapper.
