Author: Laszlo Kajan <lkajan@rostlab.org>
Description: do not attempt to remove log file when it is "/dev/null"
 Silencing logging is only possible by setting the log file to "/dev/null",
 but there is an error when it tries to remove this file, and it can not.
 This patch checks the log file name, and "/dev/null" is not removed.
Forwarded: http://lists.alioth.debian.org/pipermail/debian-med-packaging/2012-September/017173.html
--- a/cif-parser/include/CifParserBase.h
+++ b/cif-parser/include/CifParserBase.h
@@ -73,7 +73,9 @@ class CifParser : public CifScanner
         **  \param[in] fileName - relative or absolute name of the CIF file
         **    that is to be parsed.
         **  \param[in] parseLogFileName - relative or absolute name of the file
-        **    where parsing log is to be stored.
+        **    where parsing log is to be stored. Defaults to
+        **    "<input-file>-parser.log" if left empty.
+        **    Use "/dev/null" to disable logging.
         **  \param[out] diagnostics - parsing result. If empty, parsing
         **    completed with no warnings or errors. If non-empty, there were
         **    parsing warnings and/or parsing errors.
--- a/cif-parser/src/CifParserBase.C
+++ b/cif-parser/src/CifParserBase.C
@@ -148,7 +148,7 @@ void CifParser::Parse(const string& file
     if (RcsbFile::IsEmpty(log))
     {
         log.close();
-        RcsbFile::Delete(logFileName);
+        if( logFileName != "/dev/null" ) RcsbFile::Delete(logFileName);
     }
     else
     {
